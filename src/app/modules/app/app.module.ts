import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { RouterModule } from '@angular/router';
import { ReCaptchaModule } from 'angular2-recaptcha';

import { routes } from './app.route';
import {
  AppComponent, NavComponent,
  LoginComponent, RegisterComponent,
  ProductListComponent, NotFoundComponent,
  PaymentComponent,
  OrderListComponent, OrderFormComponent
} from './components';

import { LibModule } from '../lib/lib.module';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    LoginComponent,
    RegisterComponent,
    ProductListComponent,
    NotFoundComponent,
    PaymentComponent,
    OrderListComponent,
    OrderFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,

    RouterModule.forRoot(routes, { useHash: true }),
    LibModule,
    ReCaptchaModule
  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
