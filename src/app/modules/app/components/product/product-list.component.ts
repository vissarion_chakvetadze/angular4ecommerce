import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppStore } from '../../../lib/store/app-store';
import { Product, Cart, CartItem, User } from '../../../lib/model';
import { CartActions } from '../../../lib/store/actions';
import { AuthenticationService } from '../../../lib/services/authentication.service';

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html'
})
export class ProductListComponent implements OnInit, OnDestroy {
  products: Product[];
  productsSub: any;
  cartSub: any;
  cart: Cart;
  userSub: any;
  user: User;

  constructor(
    private store: Store<AppStore>,
    private cartActions: CartActions,
    private authSvc: AuthenticationService) { }

  ngOnInit() {
    this.productsSub = this.store.select(s => s.visibleProducts)
      .subscribe(products => this.products = products);

    this.cartSub = this.store.select(s => s.userCart)
      .subscribe(cart => this.cart = cart);

    this.userSub = this.store.select(s => s.loggedInUser)
      .subscribe(user => this.user = user);
  }

  ngOnDestroy() {
    if (this.productsSub)
      this.productsSub.unsubscribe();
    if (this.cartSub)
      this.cartSub.unsubscribe();
    if (this.userSub)
      this.userSub.unsubscribe();
  }

  get showCart() {
    return this.authSvc && this.authSvc.isAuthenticated && this.cart;
  }
  
  get emptyCart() {
    if (!this.showCart)
      return true;

    if (!this.cart.items || this.cart.items.length == 0)
      return true;

    return false;
  }

  addToCart(productId: string) {

    let itemsCopy = this.cart.items.map(x => Object.assign({}, x));
    let existingCartItem = itemsCopy.find(i => i.productId == productId);

    if (existingCartItem) {
      existingCartItem.quantity++;
    } else {
      var cartItem = new CartItem();
      cartItem.productId = productId;
      cartItem.quantity = 1;

      itemsCopy.push(cartItem);
    }

    let data = {
      userId: this.user.$key,
      items: itemsCopy
    };

    this.store.dispatch(this.cartActions.updateCartItem(data));
  }

  decCount(productId: string) {
    let itemsCopy = this.cart.items.map(x => Object.assign({}, x));
    let existingCartItem = itemsCopy.find(i => i.productId == productId);

    if (existingCartItem) {
      existingCartItem.quantity--;
    }

    if (existingCartItem.quantity == 0) {
      this.removeFromCart(productId);
    } else {

      let data = {
        userId: this.user.$key,
        items: itemsCopy
      };

      this.store.dispatch(this.cartActions.updateCartItem(data));
    }
  }

  removeFromCart(productId: string) {
    let itemsCopy = this.cart.items.map(x => Object.assign({}, x));
    let existingCartItem = itemsCopy.find(i => i.productId == productId);
    var itemIndex = itemsCopy.findIndex(i => i.productId == productId);

    if (itemIndex > -1) {
      itemsCopy.splice(itemIndex, 1);
    }

    let data = {
      userId: this.user.$key,
      items: itemsCopy
    };

    this.store.dispatch(this.cartActions.updateCartItem(data));
  }
}