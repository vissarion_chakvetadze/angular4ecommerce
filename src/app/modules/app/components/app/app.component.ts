import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';

import { AppStore } from '../../..//lib/store/app-store';
import { ProductActions } from '../../../lib/store/actions';

import { User } from '../../../lib/model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'app';
  user: User;
  userSub: Subscription;

  constructor(
    private productActions: ProductActions,
    private store: Store<AppStore>,
    private router: Router, ) {

    this.userSub = store.select(s => s.loggedInUser).subscribe(user => {
      this.user = user
      if (user) {
        let url: string;
        this.store.take(1).subscribe(s => url = s.loginRedirectUrl);
        if (url) {
          this.router.navigate([url]);
        } else {
          this.router.navigate(['/']);
        }
      }
      else {
        //if user logsout then redirect to home page
        this.router.navigate(['/']);
      }
    });
  }

  ngOnInit() {
    this.store.dispatch(this.productActions.loadVisibleProducts());
  }

  ngOnDestroy() {
    alert('app ondestroy');
    this.userSub.unsubscribe();
  }
}
