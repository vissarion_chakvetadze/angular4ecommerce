import { Component, Input, OnInit, OnDestroy, Renderer } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppStore } from '../../../lib/store/app-store';
import { Product, Cart, CartItem, User, Payment } from '../../../lib/model';
import { OrderActions } from '../../../lib/store/actions';

@Component({
  selector: 'payment',
  templateUrl: './payment.component.html'
})
export class PaymentComponent implements OnInit, OnDestroy {
  globalListener: any;
  cartSub: any;
  cart: Cart;

  constructor(
    private store: Store<AppStore>,
    private OrderActions: OrderActions,
    private renderer: Renderer) { }

  ngOnInit() {
    this.cartSub = this.store.select(s => s.userCart)
      .subscribe(cart => this.cart = cart);
  }

  ngOnDestroy() {
    if (this.cartSub)
      this.cartSub.unsubscribe();
  }

  openCheckout() {
    let stripeConfig = {
      key: 'pk_test_IqaSBNv6fJgI0CxVQ0qIH0NI',
      locale: 'auto',
      token: ((token: any) => {
        let payment = new Payment();
        payment.email = token.email;
        payment.tokenId = token.id;
        payment.type = token.type;

        let action = this.OrderActions.createOrder({ cart: this.cart, payment: payment });
        this.store.dispatch(action);
      })
    };

    let handler = (<any>window).StripeCheckout.configure(stripeConfig);

    handler.open({
      name: 'Demo Site',
      description: '2 widgets',
      amount: this.cart.totalPrice * 100
    });

    this.globalListener = this.renderer.listenGlobal('window', 'popstate', () => {
      handler.close();
    });
  }
}