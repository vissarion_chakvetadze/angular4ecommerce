import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';

import { Store } from '@ngrx/store';
import { AppStore } from '../../../lib/store/app-store';
import { UserActions } from '../../../lib/store/actions';

const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {
    form: FormGroup;
    captchaPassed: boolean;

    constructor(private fb: FormBuilder,
        private store: Store<AppStore>,
        private userActions: UserActions) { }

    ngOnInit() {

        let controlsConfig = {
            email: ['', Validators.compose([Validators.required, Validators.pattern(EMAIL_REGEXP)])],
            password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
            confirmPassword: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
        };

        let extraValidation = { validator: signupFormValidator }

        this.form = this.fb.group(controlsConfig, extraValidation);
    }

    onSignupSubmit() {
        let email = this.form.get('email').value;
        let pwd = this.form.get('password').value;

        this.store.dispatch(this.userActions.register(email, pwd));
    }

    handleCorrectCaptcha(eventData) {
        if (eventData) {
            this.captchaPassed = true;
        }
    }
}


function signupFormValidator(fg: FormGroup): { [key: string]: boolean } {
    //TODO: check if email is already taken

    //Password match validation
    if (fg.get('password').value !== fg.get('confirmPassword').value)
        return { 'passwordmismatch': true }

    return null;
}