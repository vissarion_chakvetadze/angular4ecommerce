import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';

import { Store } from '@ngrx/store';
import { AppStore } from '../../../lib/store/app-store';
import { UserActions } from '../../../lib/store/actions';

const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
    form: FormGroup;

    constructor(private fb: FormBuilder,
        private store: Store<AppStore>,
        private userActions: UserActions) { }

    ngOnInit() {

        let controlsConfig = {
            email: ['', Validators.compose([Validators.required, Validators.pattern(EMAIL_REGEXP)])],
            password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
        };

        this.form = this.fb.group(controlsConfig);
    }

    onSigninSubmit() {
        let email = this.form.get('email').value;
        let password = this.form.get('password').value;

        this.store.dispatch(this.userActions.login(email, password));
    }
}