import { AppComponent } from './app/app.component';
import { NavComponent } from './nav/nav.component';
import { LoginComponent } from './account/login.component';
import { RegisterComponent } from './account/register.component';
import { ProductListComponent } from './product/product-list.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PaymentComponent } from './payment/payment.component';
import { OrderListComponent } from './order/order-list.component';
import { OrderFormComponent } from './order/order-form.component';

export {
    AppComponent,
    NavComponent,
    LoginComponent,
    RegisterComponent,
    ProductListComponent,
    NotFoundComponent,
    PaymentComponent,
    OrderListComponent,
    OrderFormComponent
};

export default [
    AppComponent,
    NavComponent,
    LoginComponent,
    RegisterComponent,
    ProductListComponent,
    NotFoundComponent,
    PaymentComponent,
    OrderListComponent,
    OrderFormComponent
];