import { Component, OnDestroy } from '@angular/core';

import { Store } from '@ngrx/store';
import { AppStore } from '../../../lib/store/app-store';

import { AuthenticationService } from '../../../lib/services';
import { User } from '../../../lib/model';

@Component({
    selector: 'app-nav',
    templateUrl: './nav.component.html',
})
export class NavComponent implements OnDestroy {
    user: User;
    userSub: any;

    constructor(
        private authService: AuthenticationService,
        private store: Store<AppStore>) {
        this.userSub = store.select(s => s.loggedInUser).subscribe(user => this.user = user);
    }

    ngOnDestroy() {
        if (this.userSub)
            this.userSub.unsubscribe();
    }

    login() {
        this.authService.ensureLogin();
    }

    register() {
        this.authService.showRegister();
    }

    logout() {
        this.authService.logout();
    }

    get isAdmin() {
        if (!this.authService)
            return false;

        return this.authService.isAdmin;
    }

    get isManager() {
        if (!this.authService)
            return false;

        return this.authService.isManager;
    }
}
