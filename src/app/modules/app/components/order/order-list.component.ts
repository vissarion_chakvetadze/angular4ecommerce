import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppStore } from '../../../lib/store/app-store';
import { Order, User } from '../../../lib/model';
import { OrderActions } from '../../../lib/store/actions';

@Component({
    selector: 'order-list',
    templateUrl: './order-list.component.html'
})
export class OrderListComponent implements OnInit, OnDestroy {
    orders: Order[];
    ordersSub: any;
    userSub: any;
    user: User;

    constructor(
        private store: Store<AppStore>,
        private orderActions: OrderActions) { }

    ngOnInit() {
        this.ordersSub = this.store.select(s => s.userOrders)
            .subscribe(orders => this.orders = orders);

        this.userSub = this.store.select(s => s.loggedInUser)
            .subscribe(user => {
                this.user = user;
                this.store.dispatch(this.orderActions.loadUserOrders(this.user.$key));
            });
    }

    ngOnDestroy() {
        if (this.ordersSub)
            this.ordersSub.unsubscribe();
        if (this.userSub)
            this.userSub.unsubscribe();
    }
}