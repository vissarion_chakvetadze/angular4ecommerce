import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';

import { AppStore } from '../../../lib/store/app-store';
import { Order, User } from '../../../lib/model';
import { OrderActions } from '../../../lib/store/actions';

@Component({
    selector: 'order-form',
    templateUrl: './order-form.component.html'
})
export class OrderFormComponent implements OnInit, OnDestroy {
    order: Order;
    orderSub: any;
    routeSub: any;
    id: string;

    constructor(
        private store: Store<AppStore>,
        private orderActions: OrderActions,
        private route: ActivatedRoute, ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.id = params['id'];
            if (this.id) {
                this.store.dispatch(this.orderActions.loadOrder(this.id));
                this.orderSub = this.store.select(s => s.userOrder)
                    .subscribe(order => this.order = order);
            } else {
                this.order = new Order();
            }
        });
    }

    ngOnDestroy() {
        if (this.orderSub)
            this.orderSub.unsubscribe();
        if (this.routeSub)
            this.routeSub.unsubscribe();
    }
}