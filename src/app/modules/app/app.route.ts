import { Routes, RouterModule } from '@angular/router';
import {
    RegisterComponent, LoginComponent,
    ProductListComponent, NotFoundComponent,
    PaymentComponent, OrderListComponent, OrderFormComponent
} from './components';

import { LoggedInAuthGuard } from '../lib/services/logged-in-auth-guard';
import { AdminAuthGuard } from '../lib/services/admin-auth-guard';

export const routes: Routes = [
    {
        path: '',
        redirectTo: '/products',
        pathMatch: 'full'
    },
    {
        path: 'products',
        component: ProductListComponent
    },
    {
        path: 'account/login',
        component: LoginComponent
    },
    {
        path: 'account/register',
        component: RegisterComponent
    },
    {
        path: 'payment',
        component: PaymentComponent
    },
    {
        path: 'orders',
        children: [
            {
                path: '',
                component: OrderListComponent
            },
            {
                path: ':id',
                component: OrderFormComponent
            }
        ]
    },
    {
        path: 'admin',
        loadChildren: 'app/modules/admin/admin.module',
        canActivate: [AdminAuthGuard],
        canLoad: [AdminAuthGuard]
    },
    {
        path: '**',
        component: NotFoundComponent
    }
];