import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Router } from '@angular/router';

import { ActionWithPayload, UserActions } from '../actions';
import { UserService, AuthenticationService, CartService } from '../../services'
import { User } from '../../model';

@Injectable()
export class UserEffects {
    constructor(
        private actions$: Actions,
        private userActions: UserActions,
        private userSvc: UserService,
        private authSvc: AuthenticationService,
        private cartSvc: CartService,
        private router: Router
    ) { }

    @Effect()
    login$ = this.actions$
        .ofType(UserActions.LOGIN)
        .map((action: ActionWithPayload<{ email: string, password: string }>) => action.payload)
        .switchMap((data: { email: string, password: string }) => this.authSvc.login(data.email, data.password))
        .filter(() => false);
    //.do((user: User) => console.log(user))

    @Effect()
    loadUserRoles$ = this.actions$
        .ofType(UserActions.LOGIN_SUCCESS)
        .map((action: ActionWithPayload<User>) => action.payload)
        .switchMap((user: User) => this.userSvc.getUserRoles(user))
        //.switchMap((user: User) => this.cartSvc.loadUserCart(user))
        .map((user: User) => this.userActions.loadUserRolesSuccess(user));

    @Effect()
    register$ = this.actions$
        .ofType(UserActions.REGISTER)
        .map((action: ActionWithPayload<{ email: string, password: string }>) => action.payload)
        .flatMap((data: { email: string, password: string }) => this.authSvc.register(data.email, data.password))
        .switchMap(afUser => this.userSvc.addUser(new User(afUser)))
        //.map((user: User) => this.userSvc.addUser(user))
        .filter(() => false);

    @Effect()
    loadUsers$ = this.actions$
        .ofType(UserActions.LOAD_USERS)
        .switchMap(() => this.userSvc.getUsers())
        .map((users: User[]) => this.userActions.loadUsersSuccess(users));

    @Effect()
    loadUser$ = this.actions$
        .ofType(UserActions.LOAD_USER)
        .map((action: ActionWithPayload<string>) => action.payload)
        .switchMap((userId: string) => this.userSvc.getUser(userId))
        .map((user: User) => this.userActions.loadUserSuccess(user));

    @Effect()
    updateUser$ = this.actions$
        .ofType(UserActions.UPDATE_USER)
        .map((action: ActionWithPayload<User>) => action.payload)
        .switchMap((user: User) => this.userSvc.updateUser(user))
        .do(() => this.router.navigate(['admin/users']))
        .map(() => this.userActions.updateUserSuccess());

    /*@Effect()
    createUser$ = this.actions$
        .ofType(UserActions.CREATE_USER)
        .map((action: ActionWithPayload<User>) => action.payload)
        .map((user: User) => this.userSvc.addUser(user))
        .do((payload: User) => this.svc.addUser(payload))
        .do((payload: User) => this.svc.)*/
}