import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Router } from '@angular/router';

import { AppStore } from '../app-store';
import { ActionWithPayload, ProductActions } from '../actions';
import { Product } from '../../model';
import { ProductService } from '../../services';

@Injectable()
export class ProductEffects {
    constructor(
        private actions$: Actions,
        private productActions: ProductActions,
        private svc: ProductService,
        private router: Router
    ) { }

    @Effect()
    loadProducts$ = this.actions$
        .ofType(ProductActions.LOAD_PRODUCTS)
        .switchMap(() => this.svc.getProducts())
        .map((products: Product[]) => this.productActions.loadProductsSuccess(products))

    @Effect()
    loadVisibleProducts$ = this.actions$
        .ofType(ProductActions.LOAD_VISIBLE_PRODUCTS)
        .switchMap(() => this.svc.getVisibleProducts())
        .map((products: Product[]) => this.productActions.loadVisibleProductsSuccess(products))

    @Effect()
    loadProduct = this.actions$
        .ofType(ProductActions.LOAD_PRODUCT)
        .map((action: ActionWithPayload<string>) => action.payload)
        .switchMap((id: string) => this.svc.getProduct(id))
        .map((product: Product) => this.productActions.loadProductSuccess(product))

    @Effect()
    addProduct = this.actions$
        .ofType(ProductActions.ADD_PRODUCT)
        .map((action: ActionWithPayload<Product>) => action.payload)
        .switchMap((product: Product) => this.svc.createProduct(product))
        .do(() => this.router.navigate(['admin/products']))
        .map(() => this.productActions.addProductSuccess());

    @Effect()
    updateProduct = this.actions$
        .ofType(ProductActions.UPDATE_PRODUCT)
        .map((action: ActionWithPayload<Product>) => action.payload)
        .switchMap((product: Product) => this.svc.updateProduct(product))
        .do(() => this.router.navigate(['admin/products']))
        .map(() => this.productActions.updateProductSuccess());
}