import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Router } from '@angular/router';

import { AppStore } from '../app-store';
import { ActionWithPayload, OrderActions } from '../actions';
import { Order, Cart, Payment } from '../../model';
import { OrderService, CartService } from '../../services';

@Injectable()
export class OrderEffects {
    constructor(
        private actions$: Actions,
        private orderActions: OrderActions,
        private orderSvc: OrderService,
        private cartSvc: CartService,
        private router: Router
    ) { }

    @Effect()
    loadOrders = this.actions$
        .ofType(OrderActions.LOAD_ORDERS)
        .switchMap(() => this.orderSvc.loadOrders())
        .map((orders: Order[]) => this.orderActions.loadOrdersSuccess(orders))

    @Effect()
    loadUserOrders = this.actions$
        .ofType(OrderActions.LOAD_USER_ORDERS)
        .map((action: ActionWithPayload<string>) => action.payload)
        .switchMap((userId: string) => this.orderSvc.loadUserOrders(userId))
        .map((orders: Order[]) => this.orderActions.loadUserOrdersSuccess(orders))

    @Effect()
    loadOrder = this.actions$
        .ofType(OrderActions.LOAD_ORDER)
        .map((action: ActionWithPayload<string>) => action.payload)
        .switchMap((orderId: string) => this.orderSvc.loadOrder(orderId))
        .map((order: Order) => this.orderActions.loadOrderSuccess(order))

    @Effect()
    createOrder$ = this.actions$
        .ofType(OrderActions.CREATE_ORDER)
        .map((action: ActionWithPayload<{ cart: Cart, payment: Payment }>) => action.payload)
        .switchMap((data: { cart: Cart, payment: Payment }) => this.orderSvc.createOrder(data.cart, data.payment)
            .map(() => this.cartSvc.updateCart(data.cart.userId, null)))
        .do(() => this.router.navigate(['products']))
        .map(() => this.orderActions.createOrderSuccess());

    @Effect()
    setOrderStatus$ = this.actions$
        .ofType(OrderActions.SET_ORDER_STATUS)
        .map((action: ActionWithPayload<{ orderId: string, status: string }>) => action.payload)
        .switchMap((data: { orderId: string, status: string }) => this.orderSvc.setOrderStatus(data.orderId, data.status))
        .map(() => this.orderActions.setOrderStatusSuccess());

    @Effect()
    removeOrder$ = this.actions$
        .ofType(OrderActions.REMOVE_ORDER)
        .map((action: ActionWithPayload<string>) => action.payload)
        .do(() => this.router.navigate(['admin/orders']))
        .switchMap((orderId: string) => this.orderSvc.removeOrder(orderId))
        .map(() => this.orderActions.removeOrderSuccess());
}