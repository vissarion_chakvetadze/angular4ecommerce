import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';

import { AppStore } from '../app-store';
import { ActionWithPayload, CartActions } from '../actions';
import { Cart, CartItem, User } from '../../model';
import { CartService } from '../../services';

@Injectable()
export class CartEffects {
    constructor(
        private actions$: Actions,
        private cartActions: CartActions,
        private svc: CartService
    ) { }

    @Effect()
    loadUserCart$ = this.actions$
        .ofType(CartActions.LOAD_CART)
        .map((action: ActionWithPayload<string>) => action.payload)
        .switchMap((userId: string) => this.svc.loadUserCart(userId))
        .map((cart: Cart) => this.cartActions.loadCartSuccess(cart));

    @Effect()
    updateCartItem$ = this.actions$
        .ofType(CartActions.UPDATE_CART_ITEM)
        .map((action: ActionWithPayload<{ userId: string, items: CartItem[] }>) => action.payload)
        .switchMap((data: { userId: string, items: CartItem[] }) => this.svc.updateCart(data.userId, data.items))
        .map(() => this.cartActions.updateCartItemSuccess())
}