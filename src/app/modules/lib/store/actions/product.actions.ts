import { Injectable } from '@angular/core';
import { Product } from '../../model';
import { ActionWithPayload } from './action-with-payload';

@Injectable()
export class ProductActions {
    static LOAD_PRODUCTS = 'LOAD_PRODUCTS';
    loadProducts(): ActionWithPayload<null> {
        return {
            type: ProductActions.LOAD_PRODUCTS,
            payload: null
        };
    }

    static LOAD_PRODUCTS_SUCCESS = 'LOAD_PRODUCTS_SUCCESS';
    loadProductsSuccess(products: Product[]): ActionWithPayload<Product[]> {
        return {
            type: ProductActions.LOAD_PRODUCTS_SUCCESS,
            payload: products
        };
    }

    static LOAD_VISIBLE_PRODUCTS = 'LOAD_VISIBLE_PRODUCTS';
    loadVisibleProducts(): ActionWithPayload<null> {
        return {
            type: ProductActions.LOAD_VISIBLE_PRODUCTS,
            payload: null
        }
    }

    static LOAD_VISIBLE_PRODUCTS_SUCCESS = 'LOAD_VISIBLE_PRODUCTS_SUCCESS';
    loadVisibleProductsSuccess(products: Product[]): ActionWithPayload<Product[]> {
        return {
            type: ProductActions.LOAD_VISIBLE_PRODUCTS_SUCCESS,
            payload: products
        }
    }

    static LOAD_PRODUCT = 'LOAD_PRODUCT';
    loadProduct(id: string): ActionWithPayload<string> {
        return {
            type: ProductActions.LOAD_PRODUCT,
            payload: id
        };
    }

    static LOAD_PRODUCT_SUCCESS = 'LOAD_PRODUCT_SUCCESS';
    loadProductSuccess(product: Product): ActionWithPayload<Product> {
        return {
            type: ProductActions.LOAD_PRODUCT_SUCCESS,
            payload: product
        };
    }

    static ADD_PRODUCT = 'ADD_PRODUCT';
    addProduct(product: Product): ActionWithPayload<Product> {
        return {
            type: ProductActions.ADD_PRODUCT,
            payload: product
        };
    }

    static ADD_PRODUCT_SUCCESS = 'ADD_PRODUCT_SUCCESS';
    addProductSuccess(): ActionWithPayload<null> {
        return {
            type: ProductActions.ADD_PRODUCT_SUCCESS,
            payload: null
        };
    }

    static UPDATE_PRODUCT = "UPDATE_PRODUCT";
    updateProduct(product: Product): ActionWithPayload<Product> {
        return {
            type: ProductActions.UPDATE_PRODUCT,
            payload: product
        };
    }

    static UPDATE_PRODUCT_SUCCESS = "UPDATE_PRODUCT_SUCCESS";
    updateProductSuccess(): ActionWithPayload<null> {
        return {
            type: ProductActions.UPDATE_PRODUCT_SUCCESS,
            payload: null
        };
    }
}