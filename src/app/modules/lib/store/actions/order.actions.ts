import { Injectable } from '@angular/core';
import { Order, Cart, Payment } from '../../model';
import { ActionWithPayload } from './action-with-payload';

@Injectable()
export class OrderActions {

    static LOAD_ORDERS = 'LOAD_ORDERS';
    loadOrders(): ActionWithPayload<null> {
        return {
            type: OrderActions.LOAD_ORDERS,
            payload: null
        }
    }

    static LOAD_ORDERS_SUCCESS = 'LOAD_ORDERS_SUCCESS';
    loadOrdersSuccess(orders: Order[]): ActionWithPayload<Order[]> {
        return {
            type: OrderActions.LOAD_ORDERS_SUCCESS,
            payload: orders
        }
    }

    static LOAD_USER_ORDERS = 'LOAD_USER_ORDERS';
    loadUserOrders(userId: string): ActionWithPayload<string> {
        return {
            type: OrderActions.LOAD_USER_ORDERS,
            payload: userId
        }
    }

    static LOAD_USER_ORDERS_SUCCESS = 'LOAD_USER_ORDERS_SUCCESS';
    loadUserOrdersSuccess(orders: Order[]): ActionWithPayload<Order[]> {
        return {
            type: OrderActions.LOAD_USER_ORDERS_SUCCESS,
            payload: orders
        }
    }

    static LOAD_ORDER = 'LOAD_ORDER';
    loadOrder(orderId: string): ActionWithPayload<string> {
        return {
            type: OrderActions.LOAD_ORDER,
            payload: orderId
        }
    }

    static LOAD_ORDER_SUCCESS = 'LOAD_ORDER_SUCCESS';
    loadOrderSuccess(order: Order): ActionWithPayload<Order> {
        return {
            type: OrderActions.LOAD_ORDER_SUCCESS,
            payload: order
        };
    }

    static CREATE_ORDER = 'CREATE_ORDER';
    createOrder(data: { cart: Cart, payment: Payment }): ActionWithPayload<{ cart: Cart, payment: Payment }> {
        return {
            type: OrderActions.CREATE_ORDER,
            payload: data
        };
    }

    static CREATE_ORDER_SUCCESS = 'CREATE_ORDER_SUCCESS';
    createOrderSuccess(): ActionWithPayload<null> {
        return {
            type: OrderActions.CREATE_ORDER_SUCCESS,
            payload: null
        };
    }

    static SET_ORDER_STATUS = 'SET_ORDER_STATUS';
    setOrderStatus(orderId: string, status: string): ActionWithPayload<{ orderId: string, status: string }> {
        return {
            type: OrderActions.SET_ORDER_STATUS,
            payload: { orderId: orderId, status: status }
        }
    }

    static SET_ORDER_STATUS_SUCCESS = 'SET_ORDER_STATUS_SUCCESS';
    setOrderStatusSuccess(): ActionWithPayload<null> {
        return {
            type: OrderActions.SET_ORDER_STATUS_SUCCESS,
            payload: null
        }
    }

    static REMOVE_ORDER = 'REMOVE_ORDER';
    removeOrder(orderId: string): ActionWithPayload<string> {
        return {
            type: OrderActions.REMOVE_ORDER,
            payload: orderId
        }
    }

    static REMOVE_ORDER_SUCCESS = 'REMOVE_ORDER_SUCCESS';
    removeOrderSuccess(): ActionWithPayload<null> {
        return {
            type: OrderActions.REMOVE_ORDER_SUCCESS,
            payload: null
        }
    }
}