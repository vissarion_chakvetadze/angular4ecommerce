import { ActionWithPayload } from './action-with-payload';
import { ProductActions } from './product.actions';
import { UserActions } from './user.actions';
import { CartActions } from './cart.actions';
import { UIStateActions } from './ui-state.actions';
import { OrderActions } from './order.actions';

export {
    ActionWithPayload,
    ProductActions,
    UserActions,
    CartActions,
    UIStateActions,
    OrderActions
};

export default [
    ProductActions,
    UserActions,
    CartActions,
    UIStateActions,
    OrderActions
];
