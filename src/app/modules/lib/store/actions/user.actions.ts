import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { ActionWithPayload } from './action-with-payload';

import { User } from '../../model';

@Injectable()
export class UserActions {

    static LOGIN = 'LOGIN';
    login(email: string, password: string): ActionWithPayload<{ email: string, password: string }> {
        return {
            type: UserActions.LOGIN,
            payload: {
                email: email,
                password: password
            }
        };
    }

    static LOGIN_SUCCESS = 'LOGIN_SUCCESS';
    loginSuccess(user: User): ActionWithPayload<User> {
        return {
            type: UserActions.LOGIN_SUCCESS,
            payload: user
        };
    }

    static LOAD_USER_ROLES_SUCCESS = 'LOAD_USER_ROLES_SUCCESS';
    loadUserRolesSuccess(user: User): ActionWithPayload<User> {
        return {
            type: UserActions.LOAD_USER_ROLES_SUCCESS,
            payload: user
        };
    }

    static LOGOFF = 'LOGOFF';
    logoff(): ActionWithPayload<null> {
        return {
            type: UserActions.LOGOFF,
            payload: null
        };
    }

    static REGISTER = 'REGISTER';
    register(email: string, password: string): ActionWithPayload<{ email: string, password: string }> {
        return {
            type: UserActions.REGISTER,
            payload: {
                email: email,
                password: password
            }
        };
    }

    static REGISTER_SUCCESS = 'REGISTER_SUCCESS';
    registerSuccess(user: User): ActionWithPayload<User> {
        return {
            type: UserActions.REGISTER_SUCCESS,
            payload: user
        };
    }

    static LOAD_USERS = 'LOAD_USERS';
    loadUsers(): ActionWithPayload<null> {
        return {
            type: UserActions.LOAD_USERS,
            payload: null
        };
    }

    static LOAD_USERS_SUCCESS = 'LOAD_USERS_SUCCESS';
    loadUsersSuccess(users: User[]): ActionWithPayload<User[]> {
        return {
            type: UserActions.LOAD_USERS_SUCCESS,
            payload: users
        };
    }

    static LOAD_USER = 'LOAD_USER';
    loadUser(id: string): ActionWithPayload<string> {
        return {
            type: UserActions.LOAD_USER,
            payload: id
        };
    }

    static LOAD_USER_SUCCESS = 'LOAD_USER_SUCCESS';
    loadUserSuccess(user: User): ActionWithPayload<User> {
        return {
            type: UserActions.LOAD_USER_SUCCESS,
            payload: user
        };
    }

    static CREATE_USER = 'CREATE_USER';
    createUser(user: User): ActionWithPayload<User> {
        return {
            type: UserActions.CREATE_USER,
            payload: user
        };
    }

    static CREATE_USER_SUCCESS = 'CREATE_USER_SUCCESS';
    createUserSuccess(): ActionWithPayload<null> {
        return {
            type: UserActions.CREATE_USER_SUCCESS,
            payload: null
        };
    }

    static UPDATE_USER = 'UPDATE_USER';
    updateUser(user: User): ActionWithPayload<User> {
        return {
            type: UserActions.UPDATE_USER,
            payload: user
        };
    }

    static UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS';
    updateUserSuccess(): ActionWithPayload<null> {
        return {
            type: UserActions.UPDATE_USER_SUCCESS,
            payload: null
        };
    }
}
