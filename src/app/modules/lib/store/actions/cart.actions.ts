import { Injectable } from '@angular/core';
import { Cart, CartItem } from '../../model';
import { ActionWithPayload } from './action-with-payload';

@Injectable()
export class CartActions {
    static LOAD_CART = 'LOAD_CART';
    loadCart(userId: string): ActionWithPayload<string> {
        return {
            type: CartActions.LOAD_CART,
            payload: userId
        };
    };

    static LOAD_CART_SUCCESS = 'LOAD_CART_SUCCESS';
    loadCartSuccess(cart: Cart): ActionWithPayload<Cart> {
        return {
            type: CartActions.LOAD_CART_SUCCESS,
            payload: cart
        };
    };

    static UPDATE_CART_ITEM = 'UPDATE_CART_ITEM';
    updateCartItem(data: { userId: string, items: CartItem[] }): ActionWithPayload<{ userId: string, items: CartItem[] }> {
        return {
            type: CartActions.UPDATE_CART_ITEM,
            payload: data
        };
    }

    static UPDATE_CART_ITEM_SUCCESS = 'UPDATE_CART_ITEM_SUCCESS';
    updateCartItemSuccess(): ActionWithPayload<null> {
        return {
            type: CartActions.UPDATE_CART_ITEM_SUCCESS,
            payload: null
        };
    }
}