import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';

import { ActionWithPayload, ProductActions } from '../actions';
import { Product } from '../../model';

import { merge, without, clone, find } from 'lodash';

export function products(state: any = [], action: ActionWithPayload<Product[]>): Product[] {
    switch (action.type) {
        case ProductActions.LOAD_PRODUCTS_SUCCESS:
            return action.payload;
        default:
            return state;
    }
};

export function visibleProducts(state: any = [], action: ActionWithPayload<Product[]>): Product[] {
    switch (action.type) {
        case ProductActions.LOAD_VISIBLE_PRODUCTS_SUCCESS:
            return action.payload;
        default:
            return state;
    }
}

export function product(state: any = {}, action: ActionWithPayload<Product>): Product {
    switch (action.type) {
        case ProductActions.LOAD_PRODUCT_SUCCESS:
            return action.payload;
        case ProductActions.ADD_PRODUCT_SUCCESS:
        case ProductActions.UPDATE_PRODUCT_SUCCESS:
            return null;
        default:
            return state;
    }
}