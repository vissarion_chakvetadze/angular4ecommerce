import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';

import { ActionWithPayload, CartActions, UserActions } from '../actions';
import { Cart } from '../../model';

import { merge, without, clone, find } from 'lodash';

export function cart(state: any = [], action: ActionWithPayload<Cart>): Cart {
    switch (action.type) {
        case CartActions.LOAD_CART_SUCCESS:
            return action.payload;
        case UserActions.LOGOFF:
            return null;
        default:
            return state;
    }
};