import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';

import { ActionWithPayload, UserActions } from '../actions';
import { User } from '../../model';

export function loggedInUser(state: any = null, action: ActionWithPayload<User>): User {
    switch (action.type) {
        case UserActions.LOGOFF:
            return null;
        case UserActions.LOGIN_SUCCESS:
        case UserActions.LOAD_USER_ROLES_SUCCESS:
        case UserActions.REGISTER:
            return action.payload;
        default:
            return state;
    }
}

export function users(state: any = [], action: ActionWithPayload<User[]>): User[] {
    switch (action.type) {
        case UserActions.LOAD_USERS_SUCCESS:
            return action.payload;
        default:
            return state;
    }
};

export function user(state: any = [], action: ActionWithPayload<User>): User {
    switch (action.type) {
        case UserActions.LOAD_USER_SUCCESS:
            return action.payload;
        case UserActions.CREATE_USER_SUCCESS:
        case UserActions.UPDATE_USER_SUCCESS:
            return null;
        default:
            return state;
    }
}