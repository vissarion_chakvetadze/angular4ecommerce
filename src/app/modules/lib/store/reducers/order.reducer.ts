import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';

import { ActionWithPayload, OrderActions } from '../actions';
import { Order } from '../../model';

import { merge, without, clone, find } from 'lodash';

export function userOrders(state: any = [], action: ActionWithPayload<Order[]>): Order[] {
    switch (action.type) {
        case OrderActions.LOAD_USER_ORDERS_SUCCESS:
            return action.payload;
        default:
            return state;
    }
};

export function userOrder(state: any = [], action: ActionWithPayload<Order>): Order {
    switch (action.type) {
        case OrderActions.LOAD_ORDER_SUCCESS:
            return action.payload;
        default:
            return state;
    }
}

export function orders(state: any = [], action: ActionWithPayload<Order[]>): Order[] {
    switch (action.type) {
        case OrderActions.LOAD_ORDERS_SUCCESS:
            return action.payload;
        default:
            return state;
    }
}

export function order(state: any = [], action: ActionWithPayload<Order>): Order {
    switch (action.type) {
        case OrderActions.LOAD_ORDER_SUCCESS:
            return action.payload;
        default:
            return state;
    }
}