import { Product, User, Cart, Order } from '../model';
import { products, product, visibleProducts, loggedInUser, loginRedirectUrl, cart, users, user, userOrders, userOrder, orders, order } from './reducers';

import { combineReducers } from '@ngrx/store';
import { compose, ActionReducerMap } from '@ngrx/store';

export interface AppStore {
    loggedInUser: User;
    loginRedirectUrl: string;

    visibleProducts: Product[];
    products: Product[];
    product: Product;

    orders: Order[];
    order: Order;

    users: User[];
    user: User;

    userCart: Cart;
    userOrders: Order[];
    userOrder: Order;
}

export const reducer: ActionReducerMap<AppStore> = {
    loggedInUser: loggedInUser,
    loginRedirectUrl: loginRedirectUrl,

    products: products,
    visibleProducts: visibleProducts,
    product: product,

    orders: orders,
    order: order,

    users: users,
    user: user,

    userCart: cart,
    userOrders: userOrders,
    userOrder: userOrder
};