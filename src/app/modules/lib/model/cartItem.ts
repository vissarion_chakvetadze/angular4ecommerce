export class CartItem {

    productId: string;
    quantity: number;

    productName: string;
    productPrice: number;

    get subtotal() {
        if (!this.productPrice || !this.quantity)
            return 0;

        return this.productPrice * this.quantity;
    }

    static fromDbModel(dbModel: any) {
        let cartItem = new CartItem();
        
        cartItem.productId = dbModel.productId;
        cartItem.quantity = dbModel.quantity;

        return cartItem;
    }
}