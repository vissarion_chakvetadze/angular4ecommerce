export class Payment {
    $key?: number;
    email: string;
    tokenId: string;
    type: string;
}