import { Payment } from './payment';

export class OrderItem {
    productId: string;
    productName: string;
    quantity: number;
    price: number;
    subtotal: number;
}

export class Order {
    $key: string;
    userId: string;
    userName: string;
    items: OrderItem[];
    totalPrice: number;
    createDate: number;
    status: string;
    payment: Payment = new Payment();
}