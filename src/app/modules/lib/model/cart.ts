import { CartItem } from './cartItem';

export class Cart {
    $key: string;
    userId: string;
    items: CartItem[] = new Array();

    get totalPrice() {
        if (!this.items) {
            return 0;
        }

        return this.items.reduce((sum, cartItem) => sum + cartItem.subtotal, 0);
    }

    static fromDbModel(dbModel) {
        let cart = new Cart();

        cart.$key = dbModel.$key;
        cart.userId = dbModel.$key;

        if (dbModel.items) {
            cart.items = dbModel.items.map(dbCartItem => CartItem.fromDbModel(dbCartItem));
        }

        return cart;
    }
}