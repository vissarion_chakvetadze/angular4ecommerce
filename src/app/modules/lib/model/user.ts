import * as firebase from 'firebase/app';
import { Cart } from './cart';

class UserRoles {
    admin: boolean = false;
    manager: boolean = false;
}

export class User {
    $key?: string;
    email: string;
    roles: UserRoles = new UserRoles();

    constructor(authState: firebase.User) {
        if (authState) {
            this.$key = authState.uid;
            this.email = authState.providerData[0].email;
            //this.displayName = (authState.providerData[0].displayName ? authState.providerData[0].displayName : this.email);
        }
    }

    static fromDbModel(dbModel: any) {
        let user = new User(null);

        user.$key = dbModel.$key;
        user.email = dbModel.email;

        if (dbModel.roles) {
            user.roles.admin = !!dbModel.roles.admin;
            user.roles.manager = !!dbModel.roles.manager;
        }

        return user;
    }
}
