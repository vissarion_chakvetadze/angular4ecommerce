export class Product {
  $key?: number;
  name: string;
  price: number;
  hidden: boolean;
}