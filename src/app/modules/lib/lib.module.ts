import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

//import { InMemoryWebApiModule } from 'angular-in-memory-web-api/in-memory-web-api.module';
//import { InMemoryDataService } from './services/in-memory-data.service';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { LoggedInAuthGuard } from './services/logged-in-auth-guard';
import { AdminAuthGuard } from './services/admin-auth-guard';

import { ProductService, CartService, UserService, OrderService, AuthenticationService } from './services';
import { ProductActions, CartActions, UserActions, OrderActions, UIStateActions } from './store/actions';
import { ProductEffects, CartEffects, UserEffects, OrderEffects } from './store/effects';
import { reducer } from './store/app-store';

import { AngularFireModule, FirebaseAppConfig } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

export const firebaseConfig = {
  apiKey: "AIzaSyAO5G8Ubmjlp_dSlC3OW0bZDwZpc-3CbN0",
  authDomain: "ecommerce-728d2.firebaseapp.com",
  databaseURL: "https://ecommerce-728d2.firebaseio.com",
  projectId: "ecommerce-728d2",
  storageBucket: "ecommerce-728d2.appspot.com",
  messagingSenderId: "820025117883"
};

@NgModule({
  declarations: [
  ],
  imports: [
    HttpModule,

    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,

    StoreModule.forRoot(reducer),
    EffectsModule.forRoot([ProductEffects, CartEffects, UserEffects, OrderEffects]),
  ],
  providers: [
    LoggedInAuthGuard,
    AdminAuthGuard,

    ProductService,
    CartService,
    AuthenticationService,
    UserService,
    OrderService,

    ProductActions,
    UserActions,
    CartActions,
    UIStateActions,
    OrderActions
  ],
  bootstrap: []
})
export class LibModule { }
