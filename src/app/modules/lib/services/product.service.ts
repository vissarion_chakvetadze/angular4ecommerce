import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase } from 'angularfire2/database';

import { Product } from '../model';

@Injectable()
export class ProductService {
    constructor(private db: AngularFireDatabase) {
    }

    getProducts(): Observable<Product[]> {
        return this.db.list('/products');
    }

    getVisibleProducts() {
        return this.db.list('/products', {
            query: {
                orderByChild: 'hidden',
                equalTo: false
            }
        })
    }

    getProduct(id: string): Observable<Product> {
        return this.db.object('/products/' + id).take(1);
    }

    createProduct(product: Product) {
        var createRequest = this.db.list('/products').push({ name: product.name, price: product.price, hidden: false });
        return Observable.fromPromise(createRequest);
    }

    updateProduct(product: Product) {
        var updateRequest = this.db.object('/products/' + product.$key).set(product);
        return Observable.fromPromise(updateRequest);
    }
}