import { AuthenticationService } from './authentication.service';
import { ProductService } from './product.service';
import { CartService } from './cart.service';
import { UserService } from './user.service';
import { OrderService } from './order.service';

export {
    AuthenticationService,
    ProductService,
    CartService,
    UserService,
    OrderService
}

export default [
    AuthenticationService,
    ProductService,
    CartService,
    UserService,
    OrderService
]