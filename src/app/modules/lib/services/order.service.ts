import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase } from 'angularfire2/database';
import '../rxjs-extensions';

import { ProductService } from './product.service';
import { UserService } from './user.service';
import { Cart, CartItem, User, Order, OrderItem, Payment } from '../model';

@Injectable()
export class OrderService {
    constructor(
        private db: AngularFireDatabase,
        private productService: ProductService,
        private userService: UserService) {
    }

    loadOrders() {
        return this.db.list('/orders')
            .map((orders: Order[]) => {
                orders.forEach(order => {
                    order.items.forEach(item => {
                        this.productService.getProduct(item.productId).subscribe(pr => {
                            item.productName = pr.name;
                        })
                    })
                    this.userService.getUser(order.userId).subscribe(u => {
                        order.userName = u.email;
                    })
                })

                return orders;
            })
    }

    loadUserOrders(userId: string) {
        return this.db.list('/orders', {
            query: {
                orderByChild: 'userId',
                equalTo: userId
            }
        }).map((orders: Order[]) => {
            orders.forEach(order => {
                order.items.forEach(item => {
                    this.productService.getProduct(item.productId).subscribe(pr => {
                        item.productName = pr.name;
                    })
                })
                this.userService.getUser(order.userId).subscribe(u => {
                    order.userName = u.email;
                })
            })

            return orders;
        })
    }

    loadOrder(orderId: string) {
        return this.db.object('/orders/' + orderId)
            .map((order: Order) => {

                if (order.items) {
                    order.items.forEach(item => {
                        this.productService.getProduct(item.productId).subscribe(pr => {
                            item.productName = pr.name;
                        })
                    })
                }
                
                this.userService.getUser(order.userId).subscribe(u => {
                    order.userName = u.email;
                })

                return order;
            })
    }

    createOrder(cart: Cart, payment: Payment) {
        let order = new Order();
        order.totalPrice = cart.totalPrice;
        order.userId = cart.userId;
        order.createDate = Date.now();
        order.payment = payment;
        order.status = 'Paid';
        order.items = cart.items.map(cartItem => {
            let orderItem = new OrderItem();
            orderItem.price = cartItem.productPrice;
            orderItem.productId = cartItem.productId;
            orderItem.quantity = cartItem.quantity;
            orderItem.subtotal = cartItem.subtotal;
            return orderItem;
        });

        var createRequest = this.db.list('/orders/').push(order);
        return Observable.fromPromise(createRequest);
    }

    setOrderStatus(orderId: string, status: string) {
        var updateRequest = this.db.object('/orders/' + orderId + '/status').set(status);
        return Observable.fromPromise(updateRequest);
    }

    removeOrder(orderId: string) {
        var removeRequest = this.db.object('/orders/' + orderId).set(null);
        return Observable.fromPromise(removeRequest);
    }
}