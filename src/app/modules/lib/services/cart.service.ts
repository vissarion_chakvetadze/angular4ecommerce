import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase } from 'angularfire2/database';
import '../rxjs-extensions';

import { ProductService } from './product.service';
import { Cart, CartItem, User } from '../model';

@Injectable()
export class CartService {
    constructor(
        private db: AngularFireDatabase,
        private productService: ProductService) {
    }

    loadUserCart(userId: string): Observable<Cart> {
        return this.db.object('/carts/' + userId)
            .map((dbCart) => {
                
                let cart = Cart.fromDbModel(dbCart);

                cart.items.forEach(item => {
                    this.productService.getProduct(item.productId).subscribe(pr => {
                        item.productName = pr.name;
                        item.productPrice = pr.price;
                    })
                })

                return cart;
            })

    }

    updateCart(userId: string, items: CartItem[]) {
        var updateRequest = this.db.object('/carts/' + userId + '/items/').set(items);
        return Observable.fromPromise(updateRequest);
    }
}