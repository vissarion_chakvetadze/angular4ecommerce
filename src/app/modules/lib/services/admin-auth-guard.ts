import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanLoad, ActivatedRouteSnapshot, RouterStateSnapshot, Route } from '@angular/router';

import { AuthenticationService } from './authentication.service';

@Injectable()
export class AdminAuthGuard implements CanActivate, CanActivateChild, CanLoad {
    constructor(private authService: AuthenticationService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (!this.authService.isAuthenticated) {
            this.authService.showLogin(state.url);
            return false;
        }

        let hasRoles = this.authService.user.roles.admin
            || this.authService.user.roles.manager;

        if (!hasRoles) {
            this.authService.showLogin(state.url);
            return false;
        }

        return true;
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.canActivate(route, state);
    }

    canLoad(route: Route): boolean {
        if (!this.authService.isAuthenticated)
            return false;

        let hasRoles = this.authService.user.roles.admin
            || this.authService.user.roles.manager;

        if (!hasRoles)
            return false;

        return true;
    }
}