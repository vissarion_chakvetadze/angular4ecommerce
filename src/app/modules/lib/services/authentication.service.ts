import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Rx';

import { AppStore } from '../store/app-store';
import { UserActions, CartActions } from '../store/actions';
import { User } from '../model';

import '../rxjs-extensions';

@Injectable()
export class AuthenticationService {
    constructor(private store: Store<AppStore>,
        private userActions: UserActions,
        private cartActions: CartActions,
        public afAuth: AngularFireAuth,
        private router: Router) {

        this.afAuth.authState.subscribe(afUser => {
            if (afUser) {
                let user = new User(afUser);
                this.store.dispatch(this.userActions.loginSuccess(user));
                this.store.dispatch(this.cartActions.loadCart(user.$key));
            }
            else {
                // user not logged in
                this.store.dispatch(this.userActions.logoff());
            }
        });
    }

    ensureLogin = function () {
        if (!this.isAuthenticated)
            this.showLogin();
    };

    showLogin = function (url?: string) {
        this.router.navigate(['/account/login']);

    };

    showRegister = function (url?: string) {
        this.router.navigate(['/account/register']);
    };

    logout = function () {
        this.afAuth.auth.signOut();
    };

    login(email: string, password: string) {
        var loginRequest = this.afAuth.auth.signInWithEmailAndPassword(email, password);
        return Observable.from(loginRequest);
    }

    register(email: string, password: string) {
        var registerRequest = this.afAuth.auth
            .createUserWithEmailAndPassword(email, password);
        return Observable.from(registerRequest);
    }

    get isAuthenticated(): boolean {
        let user: User;
        this.store.take(1).subscribe(s => user = s.loggedInUser)
        if (user)
            return true;

        return false;
    };

    get user(): User {
        let user: User;
        this.store.take(1).subscribe(s => user = s.loggedInUser)
        return user;
    };

    get isAdmin() {
        let user = this.user;

        if (!user)
            return false;
        if (!user.roles)
            return false;

        return user.roles.admin;
    }

    get isManager() {
        let user = this.user;

        if (!user)
            return false;
        if (!user.roles)
            return false;

        return user.roles.manager;
    }
}