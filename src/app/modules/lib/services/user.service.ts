import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase } from 'angularfire2/database';

import { User } from '../model';

@Injectable()
export class UserService {
    constructor(private db: AngularFireDatabase) {
    }

    getUsers(): Observable<User[]> {
        return this.db.list('/users');
    }

    getUser(userId: string): Observable<User> {
        return this.db.object('/users/' + userId)
            .map(dbUser => {
                return User.fromDbModel(dbUser);
            });
    }

    addUser(user: User) {
        var createRequest = this.db.object('/users/' + user.$key).set({ email: user.email });
        return Observable.fromPromise(createRequest);
    }

    getUserRoles(user: User): Observable<User> {
        return this.db.object('/users/' + user.$key + "/roles")
            .take(1)
            .map(roles => {
                user.roles = roles;
                return user;
            });
    }

    updateUser(user: User) {
        var updateRequest = this.db.object('/users/' + user.$key + '/roles').set(user.roles);
        return Observable.fromPromise(updateRequest);
    }
}