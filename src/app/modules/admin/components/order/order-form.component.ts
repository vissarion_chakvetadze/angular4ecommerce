import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AppStore } from '../../../lib/store/app-store';
import { Order } from '../../../lib/model';
import { OrderActions } from '../../../lib/store/actions';
import { AuthenticationService } from '../../../lib/services/authentication.service';

@Component({
    selector: 'admin-order-form',
    templateUrl: './order-form.component.html'
})
export class AdminOrderFormComponent implements OnInit, OnDestroy {
    order: Order;
    orderSub: Subscription;
    routeSub: Subscription;
    id: string;

    constructor(
        private store: Store<AppStore>,
        private route: ActivatedRoute,
        private orderActions: OrderActions,
        private authSvc: AuthenticationService) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.id = params['id'];
            if (this.id) {
                this.store.dispatch(this.orderActions.loadOrder(this.id));
                this.orderSub = this.store.select(s => s.order).subscribe(order => this.order = order)
            } else {
                this.order = new Order();
            }
        });
    }

    ngOnDestroy() {
        if (this.orderSub)
            this.orderSub.unsubscribe();
        if (this.routeSub)
            this.routeSub.unsubscribe();
    }

    get canShip() {
        return this.order && this.order.status != 'Shipped' && this.authSvc.isManager;
    }

    shipOrder() {
        this.store.dispatch(this.orderActions.setOrderStatus(this.order.$key, 'Shipped'));
    }

    deleteOrder() {
        this.store.dispatch(this.orderActions.removeOrder(this.order.$key));
    }
}