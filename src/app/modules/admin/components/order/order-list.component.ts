import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppStore } from '../../../lib/store/app-store';
import { Order, User } from '../../../lib/model';
import { OrderActions } from '../../../lib/store/actions';

@Component({
    selector: 'admin-order-list',
    templateUrl: './order-list.component.html'
})
export class AdminOrderListComponent implements OnInit, OnDestroy {
    orders: Order[];
    ordersSub: any;

    constructor(
        private store: Store<AppStore>,
        private orderActions: OrderActions) { }

    ngOnInit() {
        this.ordersSub = this.store.select(s => s.orders)
            .subscribe(orders => this.orders = orders);

        this.store.dispatch(this.orderActions.loadOrders());
    }

    ngOnDestroy() {
        if (this.ordersSub)
            this.ordersSub.unsubscribe();
    }
}