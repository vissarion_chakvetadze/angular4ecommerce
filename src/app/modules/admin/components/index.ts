import { UserListComponent } from './user/user-list.component';
import { UserFormComponent } from './user/user-form.component';
import { AdminProductListComponent } from './product/product-list.component';
import { AdminProductFormComponent } from './product/product-form.component';
import { AdminOrderListComponent } from './order/order-list.component';
import { AdminOrderFormComponent } from './order/order-form.component';

export {
    UserListComponent,
    UserFormComponent,
    AdminProductListComponent,
    AdminProductFormComponent,
    AdminOrderListComponent,
    AdminOrderFormComponent
}

export default {
    UserListComponent,
    UserFormComponent,
    AdminProductListComponent,
    AdminProductFormComponent,
    AdminOrderListComponent,
    AdminOrderFormComponent
}