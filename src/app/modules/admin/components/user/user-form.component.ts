import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AppStore } from '../../../lib/store/app-store';
import { User } from '../../../lib/model';
import { UserActions } from '../../../lib/store/actions';

@Component({
    selector: 'admin-user-form',
    templateUrl: './user-form.component.html'
})
export class UserFormComponent implements OnInit, OnDestroy {
    user: User = new User(null);
    userSub: Subscription;
    routeSub: Subscription;
    id: string;
    form: FormGroup;

    title: string;

    constructor(
        private store: Store<AppStore>,
        private route: ActivatedRoute,
        private fb: FormBuilder,
        private userActions: UserActions) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.id = params['id'];
            if (this.id) {
                this.store.dispatch(new UserActions().loadUser(this.id));
                this.userSub = this.store.select(s => s.user).subscribe(user => this.user = user);
            }
        });
        let controlsConfig = {
            email: ['', Validators.compose([Validators.required])],
            admin: [''],
            manager: ['']
        };

        this.form = this.fb.group(controlsConfig);
        this.title = this.id ? "Edit user" : "Create user";
    }

    ngOnDestroy() {
        if (this.userSub)
            this.userSub.unsubscribe();
        if (this.routeSub)
            this.routeSub.unsubscribe();
    }

    onSubmit() {
        if (this.id) {
            this.store.dispatch(this.userActions.updateUser(this.user));
        } else {
            //this.store.dispatch(this.productActions.addProduct(this.product));
        }
    }
}