import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { AppStore } from '../../../lib/store/app-store';
import { User } from '../../../lib/model';
import { UserActions } from '../../../lib/store/actions';

@Component({
  selector: 'admin-user-list',
  templateUrl: './user-list.component.html'
})
export class UserListComponent implements OnInit, OnDestroy {
  users: User[];
  sub: any;

  constructor(
    private store: Store<AppStore>,
    private userActions: UserActions,
  ) {
  }

  ngOnInit() {
    this.store.dispatch(this.userActions.loadUsers());
    this.sub = this.store.select(s => s.users).subscribe(users => {
      this.users = users});
  }

  ngOnDestroy() {
    if (this.sub)
      this.sub.unsubscribe();
  }

}