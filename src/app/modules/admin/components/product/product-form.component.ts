import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthenticationService } from '../../../lib/services';
import { AppStore } from '../../../lib/store/app-store';
import { Product } from '../../../lib/model';
import { ProductActions } from '../../../lib/store/actions';

@Component({
    selector: 'admin-product-form',
    templateUrl: './product-form.component.html'
})
export class AdminProductFormComponent implements OnInit, OnDestroy {
    product: Product;
    productSub: Subscription;
    routeSub: Subscription;
    id: string;
    form: FormGroup;

    title: string;

    constructor(
        private store: Store<AppStore>,
        private route: ActivatedRoute,
        private fb: FormBuilder,
        private productActions: ProductActions,
        private authSvc: AuthenticationService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.id = params['id'];
            if (this.id) {
                this.store.dispatch(this.productActions.loadProduct(this.id));
                this.productSub = this.store.select(s => s.product).subscribe(product => this.product = product);
            } else {
                this.product = new Product();
            }
        });

        let controlsConfig = {
            name: ['', Validators.compose([Validators.required])],
            price: ['', Validators.compose([Validators.required])],
            hidden: ['']
        };

        this.form = this.fb.group(controlsConfig);
        this.title = this.id ? "Edit product" : "Create product";
    }

    ngOnDestroy() {
        if (this.productSub)
            this.productSub.unsubscribe();
        if (this.routeSub)
            this.routeSub.unsubscribe();
    }

    onSubmit() {
        if (this.id) {
            this.store.dispatch(this.productActions.updateProduct(this.product));
        } else {
            this.store.dispatch(this.productActions.addProduct(this.product));
        }
    }
}