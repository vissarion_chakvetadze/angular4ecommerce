import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';

import { AppStore } from '../../../lib/store/app-store';
import { ProductActions } from '../../../lib/store/actions';
import { Product } from '../../../lib/model';

@Component({
    selector: 'admin-product-list',
    templateUrl: './product-list.component.html'
})
export class AdminProductListComponent implements OnInit, OnDestroy {
    products: Product[];
    sub: Subscription;

    constructor(
        private store: Store<AppStore>,
        private productActions: ProductActions) {
    }

    ngOnInit() {
        this.store.dispatch(this.productActions.loadProducts());

        this.sub = this.store.select(s => s.products)
            .subscribe(products => this.products = products);
    }

    ngOnDestroy() {
        if (this.sub)
            this.sub.unsubscribe();
    }

}