import { Routes, RouterModule } from '@angular/router';
import {
    UserListComponent, UserFormComponent,
    AdminProductListComponent, AdminProductFormComponent,
    AdminOrderListComponent, AdminOrderFormComponent
} from './components';

export const routes: Routes = [
    {
        path: '',
        component: UserListComponent
    },
    {
        path: 'users',
        children: [
            {
                path: '',
                component: UserListComponent
            },
            {
                path: 'edit/:id',
                component: UserFormComponent
            }
        ]
    },
    {
        path: 'orders',
        children: [
            {
                path: '',
                component: AdminOrderListComponent
            },
            {
                path: ':id',
                component: AdminOrderFormComponent
            }
        ]
    },
    {
        path: 'products',
        children: [
            {
                path: '',
                component: AdminProductListComponent
            },
            {
                path: 'create',
                component: AdminProductFormComponent
            },
            {
                path: 'edit/:id',
                component: AdminProductFormComponent
            }
        ]
    }
];