import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { routes } from './admin.route';

import { UserListComponent, UserFormComponent, AdminProductListComponent, AdminProductFormComponent, AdminOrderListComponent, AdminOrderFormComponent } from './components';

@NgModule({
    declarations: [
        UserListComponent,
        UserFormComponent,
        AdminProductListComponent,
        AdminProductFormComponent,
        AdminOrderListComponent,
        AdminOrderFormComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    providers: [

    ],
    exports: [
        UserListComponent,
        AdminProductListComponent,
        RouterModule
    ]
})
export default class AdminModule { }
